jQuery(document).ready(function () {
    var submitTerminal = function() {
        var terminal = $("select#terminals").val();
        if (typeof terminal === "undefined") {
            var data = "terminal=0";
        } else {
            var data = "terminal=" + terminal;
        }
        if ($("input#cgv").is(":checked")) {
            $.ajax({
                type: "POST",
                url: baseDir + "modules/allparcels/terminals.php",
                data: data,
                dataType: "json"
            });
        }
    };

    $(document).on('change', 'input.delivery_option_radio', function() {
        $("td.terminals_list").empty();
        // carriers comes from template
        $("td.terminals_list").html(carriers[$(this).val().replace(",", "")]);
    });

    $(document).on('submit', 'form#form', submitTerminal);
    $(document).on('click', '.place_order', submitTerminal);
});
